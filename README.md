# Curriculum Vitae
[![pipeline status](https://gitlab.com/vcanalesp/cv/badges/main/pipeline.svg)](https://gitlab.com/vcanalesp/cv/-/commits/main)
<a href="https://gitlab.com/vcanalesp/cv/-/jobs/artifacts/main/browse?job=generate_pdf"><img alt="Link a la Documentación" src="https://img.shields.io/badge/cv-link-brightgreen"></a>

Un proyecto para obtener un buen currículum vitae sin esfuerzo.

## ¿ Dónde ?

[Haga clic aquí](https://gitlab.com/vcanalesp/cv/-/jobs/artifacts/main/browse?job=generate_pdf) para navegar por la última versión de mi currículum.

## ¿Por qué?
Hice este proyecto como un medio para:
   - Versionar mi CV.
   - Automatizar el proceso para actualizar mi currículum.
   - Obtenga más información sobre la integración continua.
   - Aprenda a usar [Gitlab CI/CD Pipelines](https://about.gitlab.com/product/continuous-integration/).


> **Nota**: Gracias [fralfaro](https://gitlab.com/fralfaro) por ayudarme con este proyecto!.